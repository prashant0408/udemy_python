import json


def FormatResponseMiddleware(get_response):
    def middleware(request):
        response = get_response(request)
        if response.status_code in [401, 400, 403, 503]:
            response.content = json.dumps({'errors': json.loads(response.content.decode("utf-8"))})
        return response
    return middleware

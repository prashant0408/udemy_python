
def methodOverrideMiddleware(get_response):
    def middleware(request):
        if 'HTTP_METHOD_OVERRIDE_HEADER' in request.META:
            request.method = request.META['HTTP_METHOD_OVERRIDE_HEADER']
        response = get_response(request)
        return response
    return middleware

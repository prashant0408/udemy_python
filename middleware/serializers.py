
from rest_framework import serializers as drf_serializers
from middleware.models import *



class TokenSerializer(drf_serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = '__all__'

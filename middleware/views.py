from django.shortcuts import render
from middleware.models import Token
from middleware.serializers import TokenSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

class TokenViewSet(viewsets.ViewSet):
    queryset = Token.objects
    serializer_class = TokenSerializer

    def retrieve(self, request, pk=None):
        token = Token.objects.get(token=request.META['HTTP_ACCESS_TOKEN'])
        if token is None:
            return Response({'messages': ['No such Token found']}, status=status.HTTP_400_BAD_REQUEST)
        # token.delete()
        return Response({})

    def destroy(self, request, pk=None):
        token = Token.objects.get(token=request.META['HTTP_ACCESS_TOKEN'])
        if token is None:
            return Response({'messages': ['No such Token found']}, status=status.HTTP_400_BAD_REQUEST)
        token.delete()
        return Response({})

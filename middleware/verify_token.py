from middleware.models import Token
from middleware.serializers import TokenSerializer
from django.http import HttpResponse
from rest_framework import status
import json
from django.conf import settings


def check_authenticated_path(request):
    path_info_exceptions = ['login', 'signup','admin','user/']
    for path in path_info_exceptions:
        if path in request.path_info:
            return True
    return False


def VerifyToken(get_response):
    def middleware(request):
        print(request.method)
        if not check_authenticated_path(request) and 'OPTIONS' not in request.method:
            if('HTTP_ACCESS_TOKEN' not in request.META):
                return HttpResponse(json.dumps({'accessToken': ['Access Token Missing!']}), content_type='application/json; charset=utf-8', status=status.HTTP_401_UNAUTHORIZED)

        if  check_authenticated_path(request) and 'PUT' in request.method:
            if('HTTP_ACCESS_TOKEN' not in request.META):
                return HttpResponse(json.dumps({'accessToken': ['Access Token Missing!']}), content_type='application/json; charset=utf-8', status=status.HTTP_401_UNAUTHORIZED)

        if  'user/addtopics' in request.path_info and 'OPTIONS' not in request.method:
            if('HTTP_ACCESS_TOKEN' not in request.META):
                return HttpResponse(json.dumps({'accessToken': ['Access Token Missing!']}), content_type='application/json; charset=utf-8', status=status.HTTP_401_UNAUTHORIZED)

            token = request.META['HTTP_ACCESS_TOKEN']
            token = Token.objects.get(token=token)
            if token:
                token.save()
            serializedToken = TokenSerializer(token)
            if not serializedToken.data['token']:
                return HttpResponse(json.dumps({'accessToken': ['Invalid Access Token!']}), content_type='application/json; charset=utf-8', status=status.HTTP_401_UNAUTHORIZED)
            request.auth_data = serializedToken.data
        response = get_response(request)
        return response
    return middleware

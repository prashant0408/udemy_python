from django.db import models
from user.models import User


# Create your models here.
class Token(models.Model):
    token = models.CharField(max_length=255,primary_key=True)
    userID = models.ForeignKey(
        'user.User',
        on_delete=models.CASCADE,
    )
    updatedAt = models.DateTimeField(auto_now=True)
    createdAt = models.DateTimeField(auto_now_add=True)

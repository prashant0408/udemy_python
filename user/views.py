from rest_framework import viewsets
from user.models import User,UserTopic
from topic.models import Topic
from django.shortcuts import get_object_or_404
from user.serializers import ListUserTopicSerializer,UserSerializer,CreateUserSerializer,LoginSerializer,UserLoginSerializer,UserTopicSerializer,UpdateUserSerializer
from topic.serializers import TopicSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import status
import uuid
import hashlib
from middleware.models import Token
from middleware.serializers import TokenSerializer
from oauthlib.common import generate_token




# Create your views here.

class UserViewSet(viewsets.ViewSet):
    queryset = User.objects
    serializer_class = UserSerializer

    def list(self, request):
        user = User.objects.all()
        serializer = UserSerializer(user, many=True)
        return Response({"data": serializer.data}, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        try:
            user = User.objects.get(pk=pk)
            serializer = UserSerializer(user)
            return Response({"data": serializer.data}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error':'No user found'}, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request):

        serializer =CreateUserSerializer(data=request.data)
        if serializer.is_valid():
            new_user = serializer.validated_data
            saveduser = User(**new_user)
            saveduser.save()
            return Response({"data": new_user}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request,pk=None):
        print(pk)
        serializer =UpdateUserSerializer(data=request.data)
        if serializer.is_valid():

            new_user = serializer.validated_data
            userID = new_user['userID']
            del new_user['userID']
            saved = User.objects.filter(userID=pk).update(**new_user)
            return Response({"data": saved}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    @list_route(methods=['post'], url_path='login')
    def login(self,request):
        serializer = LoginSerializer(data=request.data)
        finalData ={}
        if serializer.is_valid():
            userdata = serializer.validated_data
            try:
                saveduser = UserLoginSerializer(User.objects.get(email=userdata['email'])).data
            except Exception as e:
                print(e)
                saveduser = None
            if not saveduser:
                return Response('no user found', status=status.HTTP_401_UNAUTHORIZED)
            if not self.__check_password(saveduser['password'],userdata['password']):
                return Response('email or password is wrong', status=status.HTTP_401_UNAUTHORIZED)
            # del saveduser['password']
            newTokenSerializer = TokenSerializer(data={'userID':saveduser['userID'], 'token': self.__generateToken()})
            if newTokenSerializer.is_valid():
                newTokenSerializer.save()
                finalData = newTokenSerializer.data.copy()
                finalData['data'] = saveduser
                finalData['data'].pop('password')
                return Response(finalData,headers={'Access-Token': newTokenSerializer.data['token']},status=status.HTTP_201_CREATED)
            return Response(newTokenSerializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='signup')
    def signUp(self,request):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            userdata = serializer.validated_data
            userdata['password'] = self.__hash_password(userdata['password'])
            try:
                saveduser = User(**userdata)
                saveduser.save()
            except Exception as e:
                return Response('Email already exist', status=status.HTTP_400_BAD_REQUEST)

            return Response({"data": UserSerializer(saveduser).data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'], url_path='topics')
    def usertopic(self,request):
        try:
            id = int(request.query_params['userID'])
            usertopics = UserTopic.objects.filter(userID=id,status=True)
            serializer = ListUserTopicSerializer(usertopics,many=True);
            return Response({"data": serializer.data}, status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response({'error':'No user found'}, status=status.HTTP_404_NOT_FOUND)

    @list_route(methods=['post'], url_path='addtopics')
    def createusertopic(self,request):
        userID = int(request.data['userID'])
        topics = request.data['topics'];
        user = User.objects.get(pk=userID)
        serializer = TopicSerializer(data = topics,many=True)
        savedTopics =[]
        topicCount =0
        if serializer.is_valid():
            for topic in topics:
                usertopic = {}
                if 'status' in topic:
                    usertopic['status'] = topic['status']
                    del topic['status']
                if 'usertopicID' in topic:
                    usertopic['usertopicID'] = topic['usertopicID']
                    del topic['usertopicID']

                if usertopic['status']==True:
                    topicCount = topicCount+1
                if topicCount>6:
                    return Response('Maximum 6 topics allowed', status=status.HTTP_400_BAD_REQUEST)

                usertopic['userID'] = user
                usertopic['topicID'] = Topic(**topic)

                savedusertopic = UserTopic(**usertopic)
                savedusertopic.save()
            usertopics = UserTopic.objects.filter(userID=userID,status=True)
            serializer = ListUserTopicSerializer(usertopics,many=True);
            return Response({"data": serializer.data}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)







#TODO: can be moved into a common.py/helper.py file

    def __hash_password(self,password):
        salt = uuid.uuid4().hex
        return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ':' + salt


    def __check_password(self,hashed_password, user_password):
        password, salt = hashed_password.split(':')
        return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()

    def __generateToken(self):
        return generate_token()

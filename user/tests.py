from django.test import TestCase,SimpleTestCase
from rest_framework.test import APIRequestFactory,APITestCase
from django.urls import reverse
from rest_framework import status
from user.models import User
import uuid
import hashlib
import json
from oauthlib.common import generate_token
from middleware.models import Token
factory = APIRequestFactory()

class UserLoginAPIViewTestCase(APITestCase):
    url = '/user/login'

    def setUp(self):
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        salt = uuid.uuid4().hex
        self.password  = hashlib.sha256(salt.encode() + self.password.encode()).hexdigest() + ':' + salt
        self.user = User.objects.create(**{"email":self.email, "password":self.password})

    def test_authentication_without_password(self):
        response = self.client.post(self.url, {"email": "john@snow.com"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_wrong_password(self):
        response = self.client.post(self.url, {"email": self.email, "password": "I_know"})
        self.assertEqual(401, response.status_code)

    def test_authentication_with_valid_data(self):
        response = self.client.post(self.url, {"email": self.email, "password": "you_know_nothing"})
        self.assertEqual(201, response.status_code)
        self.assertTrue("token" in json.loads(response.content))

class UserSignUpAPIViewTestCase(APITestCase):
    url = '/user/signup'

    def setUp(self):
        self.email = "john@snow.com"
        self.password = "you_know_nothing"

    def test_signup_without_password(self):
        response = self.client.post(self.url, {"email": "john@snow.com"})
        self.assertEqual(400, response.status_code)

    def test_signup_with_invalid_email(self):
        response = self.client.post(self.url, {"email": "johnsnow.com", "password": "I_know"})
        self.assertEqual(400, response.status_code)

    def test_signup_with_valid_data(self):
        response = self.client.post(self.url, {"email": self.email, "password": "you_know_nothing"})
        self.assertEqual(201, response.status_code)

class UserDetailsAPIViewTestCase(APITestCase):
    url = '/user/'
    def setUp(self):
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.firstName = "John"
        self.lastName = "Snow"
        self.designation = 'King in the North'
        self.description = 'I am known as bastard of Ned stark but might actully be a targeryan.I know nothing but still i am the rightful heir to throne of seven kingdoms'
        salt = uuid.uuid4().hex
        self.password  = hashlib.sha256(salt.encode() + self.password.encode()).hexdigest() + ':' + salt
        self.user = User.objects.create(**{"email":self.email, "password":self.password})
        self.tokenObj = Token.objects.create(**{'userID':self.user, 'token': self.__generateToken()})


    def test_adding_details_without_designation(self):
        response = self.client.put(self.url +str(self.user.pk), {"userID":self.user.pk,"firstName":self.firstName,"lastName":self.lastName,"description":self.description},**{'HTTP_ACCESS_TOKEN':self.tokenObj.token})
        print(json.loads(response.content))
        self.assertEqual(400, response.status_code)

    def test_length_of_firstName(self):
        response = self.client.put(self.url +str(self.user.pk), {"userID":self.user.pk,"firstName":"abcdefghijklmnopqrstuvwxyz1234567","lastName":self.lastName,"designation":self.designation,"description":self.description},**{'HTTP_ACCESS_TOKEN':self.tokenObj.token})
        print(json.loads(response.content))
        self.assertEqual(400, response.status_code)

    def test_adding_valid_details_without_designation(self):
        response = self.client.put(self.url +str(self.user.pk), {"userID":self.user.pk,"firstName":self.firstName,"lastName":self.lastName,"designation":self.designation,"description":self.description},**{'HTTP_ACCESS_TOKEN':self.tokenObj.token})
        self.assertEqual(201, response.status_code)
    def test_getting_user_details(self):
        response = self.client.get(self.url +str(self.user.pk), {"userID":self.user.pk,"firstName":self.firstName,"lastName":self.lastName,"designation":self.designation,"description":self.description})
        self.assertEqual(200, response.status_code)
    def __generateToken(self):
        return generate_token()

from rest_framework import serializers as drf_serializers
from user.models import *
from topic.serializers import TopicSerializer

class UserSerializer(drf_serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('password',)


class UserTopicSerializer(drf_serializers.ModelSerializer):
    userID = UserSerializer()
    class Meta:
        model = UserTopic
        fields = '__all__'
        depth = 1

class ListUserTopicSerializer(drf_serializers.ModelSerializer):
    class Meta:
        model = UserTopic
        exclude = ('userID',)
        depth = 1

class UserLoginSerializer(drf_serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class LoginSerializer(drf_serializers.Serializer):
    email = drf_serializers.EmailField(required=True)
    password = drf_serializers.CharField(required=True)

class UpdateUserSerializer(drf_serializers.Serializer):
    userID = drf_serializers.CharField(required=True)
    firstName = drf_serializers.CharField(max_length=30, required=True)
    lastName = drf_serializers.CharField(max_length=30, required=True)
    designation = drf_serializers.CharField(max_length=64, required=True)
    description =drf_serializers.CharField(max_length=255, required=True)


class CreateUserSerializer(drf_serializers.Serializer):
    password = drf_serializers.CharField(
        write_only=True,
        required=True,
        min_length=5,
        error_messages={
            "blank": "Password cannot be empty.",
            "min_length": "Password too short.",
        },
    )

    email = drf_serializers.EmailField(required=True)
    firstName = drf_serializers.CharField(max_length=30, required=True)
    lastName = drf_serializers.CharField(max_length=30, required=True)
    designation = drf_serializers.CharField(max_length=64, required=True)
    description =drf_serializers.CharField(max_length=255, required=True)
    topics = drf_serializers.ListField(child=TopicSerializer())

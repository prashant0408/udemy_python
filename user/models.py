from django.db import models
from topic.models import Topic
# Create your models here.

class User(models.Model):
    userID = models.AutoField(primary_key=True)
    firstName = models.CharField(max_length=60,default='')
    lastName = models.CharField(max_length=60,default='')
    email = models.EmailField(max_length=200,unique=True)
    designation =models.CharField(max_length=64)
    description = models.CharField(max_length=255)
    password = models.CharField(max_length=200)
    updatedAt = models.DateTimeField(auto_now=True)
    createdAt = models.DateTimeField(auto_now_add=True)

class UserTopic(models.Model):
    usertopicID = models.AutoField(primary_key=True)
    topicID = models.ForeignKey(
        'topic.Topic',
        on_delete=models.CASCADE,
    )
    userID = models.ForeignKey(
        'User',
        on_delete=models.CASCADE,
    )
    status = models.BooleanField(default=True)

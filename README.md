# README #

### Udemy api ###

* RESTful api in Python
* 0.0.1

#### Install python3

```
#!shell

brew install python3

```


#### Install Virtualenv

```
#!shell

pip install virtualenv
virtualenv -p python3 <custom_env_name>
cd ./<custom_env_name>
source <custom_env_name>/bin/activate
```


#### Setup repo

```
#!shell

git clone https://prashant0408@bitbucket.org/prashant0408/udemy_python.git
cd udemy_python
pip install -r requirements.txt
```


#### To deactivate virtualenv

```
#!shell

deactivate
```

#### To Run Unit Tets

```
#!shell

python manage.py test
```

#### To Run the project on localhost

```
#!shell

python manage.py runserver
```
### Create Admin User
```
#!shell
python manage.py createsuperuser
```

#### Admin URL
```
#!shell
http://127.0.0.1:8000/admin
```

# Generated by Django 2.0.3 on 2018-03-31 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('topicID', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=12)),
                ('groupID', models.IntegerField(default=1)),
            ],
        ),
    ]

from django.db import models

# Create your models here.
class Topic(models.Model):
    topicID = models.AutoField(primary_key=True)
    name = models.CharField(max_length=12)
    groupID  = models.IntegerField(default=1)

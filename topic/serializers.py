
from rest_framework import serializers as drf_serializers
from topic.models import *



class TopicSerializer(drf_serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = ('topicID','name','groupID')

from topic.models import Topic
from topic.serializers import TopicSerializer
from rest_framework import viewsets
from rest_framework.response import Response
# from rest_framework.decorators import list_route
from rest_framework import status

# Create your views here.

class TopicViewSet(viewsets.ViewSet):
    queryset = Topic.objects
    serializer_class = TopicSerializer

    def list(self, request):
        topic = Topic.objects.all()
        serializer = TopicSerializer(topic, many=True)
        return Response({"data": serializer.data}, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        try:
            topic = Topic.objects.get(pk=pk)
            serializer = TopicSerializer(topic)
            return Response({"data": serializer.data}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error':'No topic found'}, status=status.HTTP_404_NOT_FOUND)

    def create(self, request):

        serializer =TopicSerializer(data=request.data)
        if serializer.is_valid():
            new_topic = serializer.validated_data
            savedtopic = Topic(**new_topic)
            print(savedtopic)
            savedtopic.save()
            return Response({"data": new_topic}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
